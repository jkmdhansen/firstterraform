provider "aws" {
  region = "${var.region}"
  profile = "${var.profile}"
}


resource "aws_security_group" "graingersg" {
  name = "${var.app}-sg"
  vpc_id = "${var.vpc}"

  ingress {
    from_port   = "${var.port_srv}"
    to_port     = "${var.port_srv}"
    protocol    = "${var.protocol_svr}"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags          = {
    Name          = "${var.app}-sg"
    BusinessUnit  = "ES"
    Environment   = "DEV"
    Service       = "${var.app}"
    Type          = "App"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "grainger_instance" {
  ami           = "${var.ami}"
  instance_type = "${var.instance}"
  vpc_security_group_ids = ["${aws_security_group.graingersg.id}"]
  subnet_id     = "${var.subnet2}"
  key_name      = "xjch006"
  tags          = {
    Name            = "${var.app}ap001"
    BusinessUnit    = "ES"
    Environment     = "DEV"
    Type            = "App"
  }
}


