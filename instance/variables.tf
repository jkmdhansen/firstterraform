variable "region" {
	description = "The name of the region to deploy into"
}

variable "profile" {
	description = "profile to use for this build"
}	

variable "protocol_elb" {
	description = "protocol for the elb to use"
	default		= ""
}	

variable "port_elb" {
	description = "protocol for the server to use"
	default		= ""
}	

variable "protocol_svr" {
	description = "protocol for the server to use"
}	

variable "port_srv" {
	description = "protocol for the server to use"
}	

variable "app" {
	description = "name of the application being deployed"
}

variable "ami" {
	description = "ami to be used for this application deployment"
}

variable "vpc" {
	description = "vpc to be used for this deployment"
}

variable "subnet1" {
	description = "first subnet to be used for this deployment"
}

variable "subnet2" {
	description = "second subnet to be used for this deployment"
}

variable "subnet3" {
	description = "third subnet to be used for this deployment"
}

variable "servername" {
	description = "server name to be used for this deployment"
}

variable "instance" {
	description = "instance type to be used for this deployment"
}
